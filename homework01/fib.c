#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "common.h"

// Calculate Fib recursively
long unsigned int calculateFibonacci(int n)
{
    if(n == 0 || n == 1){
        return n;
    }else{
        return calculateFibonacci(n - 1) + calculateFibonacci(n - 2);
    }
}

long unsigned int calculateFibonacciP(int n)
{
    int x, y;

    if(n == 0 || n == 1){
        return n;
    }else{
        #pragma omp task shared(x)
            x = calculateFibonacci(n - 1);
         #pragma omp task shared(y)
            y = calculateFibonacci(n - 2);
        #pragma omp taskwait
            return  x + y;
    }
}

void usage(int argc, char** argv)
{
    fprintf(stderr, "usage: %s <n>\n", argv[0]);
    exit(-1);
}

int main(int argc, char** argv)
{
    int n;
    if(argc > 1) {
        n = atoi(argv[1]);
    } else {
        usage(argc, argv);
    }

    // Initialize timer
    uint64_t start_t;
    uint64_t end_t;
    InitTSC();

    // Run & measure the naive recursive Fib series
    start_t = ReadTSC();
    long unsigned int fib = calculateFibonacci(n);
    end_t = ReadTSC();
    printf("Fibonacci series for %d is %lu\n", n, fib);
    printf("Time to calculate the Fibonacci series for %d is: %g\n",
           n, ElapsedTime(end_t - start_t));


    // Run & measure the parallelrecursive Fib series
    start_t = ReadTSC();
    fib = calculateFibonacciP(n);
    end_t = ReadTSC();
    printf("Parallel Fibonacci series for %d is %lu\n", n, fib);
    printf("Time to calculate the Fibonacci series in parallel for %d is: %g\n",
           n, ElapsedTime(end_t - start_t));

    return 0;
}
