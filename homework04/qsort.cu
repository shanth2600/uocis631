#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include <set>
#include <utility>
#include <string>
#include <tuple>

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/generate.h>
#include <thrust/sort.h>
#include <thrust/copy.h>


#include "common.h"

const int THREADS_PER_BLOCK = 128;
const int MAX_SEQ = THREADS_PER_BLOCK;


void usage(int argc, char** argv)
{
    if(argc < 2) {
        fprintf(stderr, "usage: %s <num elements>\n", argv[0]);
        exit(-1);
    } 
}

void init_list(int* arr, int n)
{
    assert(arr);
    srand(time(NULL));
    for(int i = 0; i < n; i++) {
        arr[i] = rand() % n; 
    }
}

int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}


 __global__ static void quickSort(int* arr, int n) {

    int idx =  threadIdx.x + blockIdx.x * blockDim.x;
    int start[MAX_SEQ];
    int end[MAX_SEQ];
    int pivot;
    int left, right;
	start[idx] = idx;
	end[idx] = n - 1;
	while (idx >= 0) {
		left = start[idx];
		right = end[idx];
		if (right > left) {
            idx--;
		}else{
            pivot = arr[left];
			while (left < right) {
				while (left < right && arr[right] >= pivot){
                    right--;
                }
				if(left < right){
                    arr[left++] = arr[right];
                }
				while (left < right && arr[left] < pivot){
                    left++;
                }
				if (left < right){
					arr[right--] = arr[left];
                }
			}
			start[idx + 1] = left + 1;
			arr[left] = pivot;
			end[idx++] = left;
			end[idx + 1] = end[idx];
			if (end[idx] - start[idx] > end[idx - 1] - start[idx - 1]) {
                int temp = start[idx];
                start[idx]  = start[idx - 1]; 
                start[idx - 1] = temp;

                int temp1 = end[idx];
                end[idx]  = end[idx - 1]; 
                end[idx - 1] = temp1;
            }
			
	}
}
 }


void output_arr(int* arr, int n){
    for(int i = 0; i < n; i++){
        printf("%i:%i\n", i, arr[i]);
    }

}

void qsort_gpu(int* arr, int n)
{
    int size = sizeof(int) * n;
    int* d_array;
    cudaMalloc((void**)&d_array, size);
    

    cudaMemcpy(d_array, arr, size, cudaMemcpyHostToDevice);
    quickSort <<< MAX_SEQ, n/MAX_SEQ >>>(d_array, n);
    cudaMemcpy(arr, d_array, size, cudaMemcpyDeviceToHost);
}


void validate(int* solution, int* answer, int n)
{
    int cnt = 0;
    for(int i = 0; i < n ; i++) {
        if(solution[i] != answer[i]) {
            cnt++;
        }
    }
    fprintf(stdout, "Found %d differences\n", cnt);
}

int main(int argc, char** argv)
{
    // check input
    usage(argc, argv);
    int n = atoi(argv[1]);
    
    fprintf(stdout, "Sorting %d values\n", n);


    // initialize timer
    uint64_t start_t;
    uint64_t end_t;
    InitTSC();

    // create input array to sort
    int* arr = (int*) AlignedMalloc(sizeof(int) * n);
    assert(arr);
    init_list(arr, n);
    int* arr_gpu = (int*) AlignedMalloc(sizeof(int) * n);
    assert(arr_gpu);
    memcpy(arr_gpu, arr, sizeof(int) * n);
     int* arr_thr = (int*) AlignedMalloc(sizeof(int) * n);
    assert(arr_thr);
    memcpy(arr_thr, arr, sizeof(int) * n);


    // -----------------------------------------------------------------  
    // sort the array using stdlib quicksort to create answer 
    start_t = ReadTSC();
    qsort(arr, n, sizeof(int), cmpfunc);
    end_t = ReadTSC();
    printf("CPU stdlib qsort time: %g\n", ElapsedTime(end_t - start_t));
    // -----------------------------------------------------------------  

    // -----------------------------------------------------------------  
    // do the quicksort on GPU (assuming integer sort)
    start_t = ReadTSC();
    qsort_gpu(arr_gpu, n);
    end_t = ReadTSC();
    printf("GPU stdlib qsort time: %g\n", ElapsedTime(end_t - start_t));

    // validate answer
    validate(arr, arr_gpu, n);
    // -----------------------------------------------------------------  

    // -----------------------------------------------------------------  
    // Use Nvidia's Thrust library to sort - likely implements radix
    // thrust::host_vector<int> h_vec(arr, arr + n);
    // thrust::device_vector<int> d_vec = h_vec;

    // start_t = ReadTSC();
    // thrust::sort(d_vec.begin(), d_vec.end());
    // end_t = ReadTSC();
    // printf("GPU Thrust qsort time: %g\n", ElapsedTime(end_t - start_t));

    // thrust::copy(d_vec.begin(), d_vec.end(), h_vec.begin());
    // std::copy(h_vec.begin(), h_vec.end(), arr_thr);

    // validate answer
    validate(arr, arr_thr, n);
    // -----------------------------------------------------------------  


    // cleanup
    AlignedFree(arr);
    AlignedFree(arr_gpu);
    AlignedFree(arr_thr);

    return 0;
}
