\documentclass[12pt]{extarticle}
\usepackage[utf8]{inputenc}
\usepackage{booktabs}
\usepackage{float}
\usepackage{listings}

\title{Estimating Pi in Parallel}
\author{Shant Hairapetian}
\date{\today}

\begin{document}

\maketitle

\section{Introduction}

Though there are multiple approaches one can use to estimate pi, the easiest way
is approximating the integral of a circle. To do this, we will use the equation of 
a circle and generate a series of rectangles which fit under that curve. By summing
the areas of each of the rectangles under the curve we begin to approximate the area
under the curve. The more rectangles used, the more accurate the estimate.

\section{Serial Implementation}
\begin{lstlisting}[language=C]
    double sum = 0;

    for(double x = 0; x < 1; x += step){
        sum += (step * circle(1, x));
    }
    
    return sum * 4;
\end{lstlisting}

The code above is a naive serial implementation of this algorithm where "step"
is one divided by the granularity (ie number of rectangles) of the approximation,
and circle is a function which given a radius and an X value, will return the Y value of
the circle centered at the origin.

\section{Parallel Implementation}

By just looking at the code we can see that the operations in the for loop are all
independent of each other (ie the value of one cell does not depend on another). This
property makes this for loop a prime candidate for parallelization. The OpenMP library
provides the "omp parallel for" pragma for automating the parallelization of for loops.
Now if the reader is hoping that all we have to do for better performance is slap a 
"\#pragma omp parallel" above out loop, the reader would be sorely disappointed. The 
first issue is that the serial implementation uses a double as the index value. This
due to the fact that we are using the unit circle and therefore steps will be some
fraction of 1. OpenMP's loop parallelization mechanism insists that the loop index
variable be an integer. In order to accomplish this we scaled the circle to be 
proportional to the granularity (num\_steps) of the approximation and used steps
of size 1. We must also specify to OpenMP that the threads spawned from the for
loop are all accumulating a single value (sum). We can express this using the 
"reduction((operations):(variable))" directive. So with all that we get:

\begin{lstlisting}[language=C]
    double sum = 0;

    #pragma omp parallel for reduction(+:sum)
    for(int x = 0; x < num_steps; x += 1){
        sum += circle(num_steps, x);
    }

    return ((double)sum/(pow(num_steps, 2))) * 4;
\end{lstlisting}

Theoretically OpenMP should chunk the processing of the for loop between the number of
cores in the system, thereby speeding up the code.

\section{Performance Analysis}
\begin{table}[H]
\centering
\begin{tabular}{@{}llll@{}}
\toprule
Steps   & Serial       & Parallel     &  \\ \midrule
10      & 1.44447e-05  & 5.08104e-04 &  \\
50      & 1.87683e-05  & 1.72543e-03  &  \\
100     & 1.56335e-05  & 1.231312e-04 &  \\
500     & 1.93688e-05  & 1.50861e-03  &  \\
1,000   & 2.06816e-05  & 5.377093e-04 &  \\
5,000   & 4.39512e-05  & 8.316001e-04 &  \\
10,000  & 7.39483e-05  & 4.62061e-04   &  \\
50,000  & 3.03448e-04 & 2.571295e-04 &  \\
100,000 & 5.92079e-04 & 5.24416e-04 &  \\ \bottomrule
\end{tabular}
\end{table}
As we can see there is something VERY wrong. The parallel implementation is an order
of magnitude slower than the serial version until they converge at around 100,000
iterations. I ran the code on a MacBook Air (dual-core), Windows 10 Machine (quad-core),
and a Digital Ocean Ubuntu VM (quad-core) all with similar results. I then modified the 
serial code to do the same scaling that the parallel version has to do to no avail.
I was able to verify (using omp\_get\_num\_threads) that mutliple threads were being
used to run the parallel code but the ONLY way I was able to get the performance 
to be comprable to the serial implementation was by forcing OpenMP to use a single
thread by using "num\_threads(1)" in the OpenMP pragma. At this point I figured that
the OpenMP may be trying to spawn a thread for each iteration of the for loop. I then
updated the code accordingly to chunk the calculation of the area into 8 distinct
chunks and then parallelize the outter loop iterating through the chunks:

\begin{lstlisting}[language=C]
    const int CHUNKS = 8;
    const int CHUNK_SIZE = num_steps / CHUNKS;
    double sum = 0.0;

    #pragma omp parallel for shared(CHUNKS, \
    CHUNK_SIZE, num_steps) reduction(+:sum)
    for(int i = 0; i < CHUNKS; i+= 1){
        sum += areaOfSubsetOfCircle(
            (i * CHUNK_SIZE), 
            ((i + 1) * CHUNK_SIZE), 
            num_steps);
    }

    return sum;
\end{lstlisting}

\begin{table}[H]
\centering
\begin{tabular}{llll}
\hline
Steps   & Serial      & Parallel (with chunking)    &  \\ \hline
10      & 1.03355e-05 & 1.64608e-04  &  \\
50      & 1.53335e-05 & 2.325382e-04 &  \\
100     & 1.70661e-05 & 1.432169e-04 &  \\
500     & 1.79823e-05 & 1.438045e-04 &  \\
1,000   & 2.08448e-05 & 4.442824e-04 &  \\
5,000   & 6.54305e-05 & 1.918608e-04 &  \\
10,000  & 7.38667e-05 & 1.632602e-03 &  \\
50,000  & 3.06559e-04 & 5.82793e-04  &  \\
100,000 & 5.98557e-04 & 5.24416-e04  &  \\ \hline
\end{tabular}
\end{table}
 
 As you can see above, this yielded similar results. It is apparent that
 I'm doing something wrong but I can't quite figure out what it is.



\end{document}
