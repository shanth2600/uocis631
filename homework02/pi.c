#include <stdlib.h> 
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include "common.h"
#include <stdint.h>
#include <time.h>

double circle(double width, double x)
{
    return sqrt(pow(width, 2) - pow(x, 2));
}

double generateSumSerial(double step)
{
    double sum = 0;

    for(double x = 0; x < 1; x += step){
        sum += (step * circle(1, x));
    }
    
    return sum * 4;
}

double areaOfSubsetOfCircle(int start, int stop, int radius)
{
    double sum = 0.0;

    for(int x = start; x < stop; x += 1){
        sum += circle(radius, x);
    }
    
    return ((double)sum/(pow(radius, 2))) * 4;
}

double generateSumParallel(int num_steps)
{
    const int CHUNKS = omp_get_max_threads();
    const int CHUNK_SIZE = num_steps / CHUNKS;
    double sum = 0.0;

    #pragma omp parallel for shared(CHUNKS, CHUNK_SIZE, num_steps) reduction(+:sum)
    for(int i = 0; i < CHUNKS; i+= 1){
        sum += areaOfSubsetOfCircle((i * CHUNK_SIZE), ((i + 1) * CHUNK_SIZE), num_steps);
    }

    return sum;
    
}


double getDecimal()
{
    return (double)rand() / (double)RAND_MAX ;
}

int sampleInsideCircle(int samples)
{
    int hits = 0;
    for(int i = 0; i < samples; i++){
        double x = getDecimal();
        double y = getDecimal();
        if(pow(x, 2) + pow(y, 2) < 1){
            hits++;
        }
    }

    return hits;
}


double generateSumMonteCarlo(int num_steps)
{
    const int CHUNKS = omp_get_max_threads();
    const int CHUNK_SIZE = num_steps / CHUNKS;
    double hits = 0.0;

    #pragma omp parallel for shared(CHUNKS, CHUNK_SIZE) reduction(+:hits)
    for(int i = 0; i < CHUNKS; i+= 1){
        hits += sampleInsideCircle(CHUNK_SIZE);
    }

    return 4.0 * hits / num_steps;

}


double calcPi(int num_steps)
{
    return generateSumSerial((double)1/num_steps);
}

double calcPiP1(int num_steps)
{
    return generateSumParallel(num_steps);
}


int main(int argc, char** argv)
{
    long num_steps = 100000;
    if(argc > 1) {
        num_steps = atoi(argv[1]);
    }


    uint64_t start_t;
    uint64_t end_t;
    InitTSC();


    start_t = ReadTSC();
    double Pi0 = calcPi(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi serially with %.1e steps is: %g\n",
           (double) num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %1.10g\n", Pi0);
    
 
    start_t = ReadTSC();
    double Pi1 = calcPiP1(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi in // with %.1e steps is: %g\n",
           (double) num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %1.10g\n", Pi1);


    return 0;
}

