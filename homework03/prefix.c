#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include "common.h"
#include "tree.h"


void usage(int argc, char** argv)
{
    fprintf(stderr, "usage: %s <num elements>\n", argv[0]);
    exit(-1);
}


void verify(int* sol, int* ans, int n)
{
    int err = 0;
    for(int i = 0; i < n; i++) {
        // printf("me: %i it:%i\n", ans[i], sol[i]);
        if(sol[i] != ans[i]) {
            err++;
        }
    }
    if(err != 0) {
        fprintf(stderr, "There was an error: %d\n", err);
    } else {
        fprintf(stdout, "Pass\n");
    }
}


void prefix_sum(int* src, int* prefix, int n)
{
    for(int i = 0; i < n; i++) {
        prefix[i] = 0;
        for(int j = 0; j <= i; j++) {
            prefix[i] += src[j];
        }
    }
}

void prefix_sum_subset(int* src, int* prefix, int start, int end)
{
    for(int i = start; i < end; i++) {
        prefix[i] = 0;
        for(int j = start; j <= i; j++) {
            prefix[i] += src[j];
        }
    }
}

void increment_subset_by(int* list, int by, int start, int end)
{
    for(int i = start; i < end; i++) {
        list[i] += by;
    }

}

void prefix_sum0(int* src, int* prefix, int n)
{
    int sum = 0;
    prefix[0] = src[0];
    for(int i = 1; i < n; i++) {
        sum += src[i - 1];
        prefix[i] = src[i] + sum;
    }
}


void prefix_sum1_scan(int* src, int* prefix, int n)
{
    memcpy(prefix, src, sizeof(int) * n);
    for(int d = 1; d < log2(n) ; d ++){
        #pragma omp parallel for shared(n, d, prefix) schedule(static)
        for(int i = 0; i < n - d; i++){
            prefix[i + d] = prefix[i] + prefix[d + i];
        }
    }
}

void outputPrefix(int* prefix, int size){
    printf("prefix: ");
    for(int i = 0; i < size; i++){
        printf("%i, ",prefix[i]);
    }
    printf("\n");
}

void prefix_sum1(int* src, int* prefix, int n)
{
    const int BLOCKS = omp_get_max_threads();
    const int BLOCK_SIZE = n / BLOCKS;
    const int REMAINDER = n % BLOCK_SIZE;
    int lastPlaceSum = 0;

    #pragma omp parallel for shared(n, src, prefix) schedule(static)
    for(int i = 0; i < n; i += BLOCK_SIZE){
        prefix_sum_subset(src, prefix, i, (i + BLOCK_SIZE));
    }

    prefix_sum_subset(src, prefix, (n - REMAINDER), n);

    for(int i = 1; i < BLOCKS; i += BLOCK_SIZE){
        lastPlaceSum += prefix[(i * BLOCK_SIZE) - 1];
        increment_subset_by(prefix, lastPlaceSum, (i * BLOCK_SIZE), ((i + 1) * BLOCK_SIZE));
    }


}

int *prefix_tree = NULL;


void instantiateTreeWithLeaves(int *src, int *tree, int n)
{
    const int TREE_SIZE = (2 * n) - 1;
    int src_index = 0;
    int tree_index;
    prefix_tree = (int *)AlignedMalloc(sizeof(int) * TREE_SIZE + 1);
    for(int i = 1; i < n + 1; i ++){
        tree_index = TREE_SIZE - n + i;
        prefix_tree[tree_index] = src[src_index];
        src_index++;
    }

}


void prefix_sum2(int *src, int *prefix, int n)
{
    instantiateTreeWithLeaves(src, prefix_tree, n);

    const int TREE_SIZE = (2 * n) - 1;
    
    int index, leftChildIndex, rightChildIndex;
    for(int d = log2(TREE_SIZE) - 1; d >= 0; d--){ 
        for(int i = (int)pow(2,d); i < pow(2,d + 1); i++){
            prefix_tree[i] = 
                prefix_tree[get_left_child(i, TREE_SIZE)] + prefix_tree[get_right_child(i, TREE_SIZE)];
        }
        
    }

    int total_sum = prefix_tree[1];
    
    prefix_tree[1] = 0;
    const int TREE_DEPTH = log2(TREE_SIZE);

    for(int d = 0; d < TREE_DEPTH; d++){ 
        const int start = pow(2,d);
        const int end = pow(2,d + 1);
        #pragma omp parallel for schedule(static) shared(prefix_tree, start, end, TREE_SIZE)
        for(int i = start; i < end; i++){
            int left_child_temp = prefix_tree[get_left_child(i, TREE_SIZE)];
            prefix_tree[get_left_child(i, TREE_SIZE)] = prefix_tree[i];
            prefix_tree[get_right_child(i, TREE_SIZE)] = prefix_tree[i] + left_child_temp;
        }
        
    }

    for(int i = 0; i < n - 1; i ++){
        prefix[i] = prefix_tree[TREE_SIZE - n + i + 2];
    }

    prefix[n - 1] = total_sum;

}


int main(int argc, char** argv)
{
    int n = 1048576;
    if(argc > 1) {
        n = atoi(argv[1]); 
    } else {
        usage(argc, argv);
    }

    int* prefix_array = (int*) AlignedMalloc(sizeof(int) * n);  
    int* input_array = (int*) AlignedMalloc(sizeof(int) * n);  
    srand(time(NULL));
    for(int i = 0; i < n; i++) {
        input_array[i] = rand() % 10;
        // input_array[i] = 1;
    }

    uint64_t start_t;
    uint64_t end_t;
    InitTSC();



    // Execute this only when N is low - otherwise, it'll take forever to run
    if(n <= 4096) {
        start_t = ReadTSC();
        prefix_sum(input_array, prefix_array, n);
        end_t = ReadTSC();
        printf("Time to do O(N^2) prefix sum on a %d element array: %g (s)\n", 
               n, ElapsedTime(end_t - start_t));
    }



    int* input_array1 = (int*) AlignedMalloc(sizeof(int) * n);  
    int* prefix_array1 = (int*) AlignedMalloc(sizeof(int) * n);  
    memcpy(input_array1, input_array, sizeof(int) * n);
    memset(prefix_array1, 0, sizeof(int) * n);
    start_t = ReadTSC();
    prefix_sum0(input_array1, prefix_array1, n);
    end_t = ReadTSC();
    printf("Time to do O(N-1) prefix sum on a %d element array: %g (s)\n", 
           n, ElapsedTime(end_t - start_t));
    verify(prefix_array, prefix_array1, n);


    memcpy(input_array1, input_array, sizeof(int) * n);
    memset(prefix_array1, 0, sizeof(int) * n);
    start_t = ReadTSC();
    prefix_sum1(input_array1, prefix_array1, n);
    end_t = ReadTSC();
    printf("Time to do O(NlogN) // prefix sum on a %d element array: %g (s)\n", 
           n, ElapsedTime(end_t - start_t));
    // verify(prefix_array, prefix_array1, n);

    

    memcpy(input_array1, input_array, sizeof(int) * n);
    memset(prefix_array1, 0, sizeof(int) * n);
    start_t = ReadTSC();
    prefix_sum2(input_array1, prefix_array1, n);
    end_t = ReadTSC();
    printf("Time to do 2(N-1) // prefix sum on a %d element array: %g (s)\n", 
           n, ElapsedTime(end_t - start_t));
    verify(prefix_array, prefix_array1, n);

    AlignedFree(prefix_tree);
    AlignedFree(prefix_array);
    AlignedFree(input_array);
    AlignedFree(input_array1);
    AlignedFree(prefix_array1);

    return 0;
}
