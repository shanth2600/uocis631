#include<stdio.h>
#include<stdlib.h>
#include<time.h>


int get_parent(int index){
	if(index > 0){
		return (index - 1)/2;
	}

	return -1;
}

int get_right_sibling(int index){
	return get_right_child(get_parent(index));
}

void preorder(int index, int* tree, int size)
{
    // checking for valid index and null node
    if(index>0)
    {
        printf("val: %i \n",tree[index]); // visiting root
        preorder(get_left_child(index, size), tree, size); //visiting left subtree
        preorder(get_right_child(index, size), tree, size); //visiting right subtree
    }
}

int get_right_child(int index, int size)
{
	// node is not null
	// and the result must lie within the number of nodes for a complete binary tree
	if ((index > 0) && (2 * index) + 1 <= size)
		return (2 * index) + 1;
	// right child doesn't exist
	return -1;
}

int get_left_child(int index, int size)
{
	// node is not null
	// and the result must lie within the number of nodes for a complete binary tree
	if ((index > 0) && ((2 * index) <= size))
		return 2 * index;
	// left child doesn't exist
	return -1;
}

int get_leftmost_leaf(int index, int size){
    int leaf_index = index;
    int ix;
    while ((ix = get_left_child(leaf_index + 1, size)) > 0){
        leaf_index = ix;
    }

    return leaf_index;
}
